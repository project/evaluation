Author: Charlie Lowe

Drupal, with its many content management and community features
(forums, blogs, books, upload, etc.), provides some basic
functionality as an elearning tool. While Drupal excels at it's
primary purpose as open, social community software, it lacks the basic
privacy features necessary for examinations and evaluations of student
work in an elearning context. That is, until the evaluation module.

The evaluation module provides private spaces for dialogue between an
instructor and students where instructors can create a prompt or
'question' and assign it to one or many students. Students can submit
their responses to the question back to the instructor. the instructor
can respond back to each student's response.

In order to provide the privacy necessary for individualized
instruction and teacher assessment of student work, the evaluation
module uses Drupal's permission system to guarantee security through
the use of three roles: instructor, tutor, and student. These roles
are configurable, allowing for multiple scenarios for question
creation and response. Not only can the teacher create questions, but
permissions may be adjusted to allow students to create questions for
other students, or even allow students to evaluate other students'
responses.

Additionally, the instructor can assign more than one question at once
or choose to have Drupal assign a random number of questions from a
set of questions. The functionality of the evaluation module is also
greatly enhanced by the ability to set time constraints. The teacher
can choose when and how long a question is visible and when and how
long a student can post a response, making it a suitable system for
timed examinations for an online learning situation.

Through the features mentioned above, by additional fine tuning of the
configuration settings, and with application of Drupal taxonomies to
questions, it is possible to achieve complex instructional goals.
Given the robust feature set and flexibility of the evaluation module,
teachers will find many creative applications of this new tool for
elearning.

The evaluation module is baed on ideas by Prof. Robert Hutterer and
was written by Gerhard Killesreiter. It currently is available for
Drupal 4.5 and requires a small patch to be applied against Drupal.

